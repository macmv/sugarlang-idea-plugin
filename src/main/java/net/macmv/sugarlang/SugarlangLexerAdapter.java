package net.macmv.sugarlang;

import com.intellij.lexer.FlexAdapter;

public class SugarlangLexerAdapter extends FlexAdapter {
  public SugarlangLexerAdapter() {
    super(new SugarlangLexer(null));
  }
}
