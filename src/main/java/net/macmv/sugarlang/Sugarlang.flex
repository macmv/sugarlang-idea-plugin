package net.macmv.sugarlang;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;

import static com.intellij.psi.TokenType.BAD_CHARACTER;
import static com.intellij.psi.TokenType.WHITE_SPACE;
import static net.macmv.sugarlang.psi.SugarlangTypes.*;

%%

%{
  public SugarlangLexer() {
    this((java.io.Reader)null);
  }
%}

%public
%class SugarlangLexer
%implements FlexLexer
%function advance
%type IElementType
%unicode

EOL=\R
WHITE_SPACE=\s+
LINE_COMMENT="//"[^\r\n]*
// The first character of an ident cannot be _ or a number
IDENT=[A-Za-z][A-Za-z0-9_]*
INT_LIT=\d+
FLOAT_LIT=\d+\.?\d+
STRING_LIT=\"[^\"]*\"

%%
<YYINITIAL> {
  "pub"    { return PUB; }
  "fn"     { return FN; }
  "struct" { return STRUCT; }
  "map"    { return MAP; }
  "if"     { return IF; }
  "else"   { return ELSE; }
  "for"    { return FOR; }
  "while"  { return WHILE; }
  "loop"   { return LOOP; }

  "{" { return OPEN_BLOCK; }
  "}" { return CLOSE_BLOCK; }
  "(" { return OPEN_PAREN ; }
  ")" { return CLOSE_PAREN; }
  "[" { return OPEN_ARR; }
  "]" { return CLOSE_ARR; }

  "=" { return ASSIGN; }
  "," { return COMMA; }
  ":" { return COLON; }
  "." { return DOT; }

  "+"  { return ADD; }
  "-"  { return SUB; }
  "*"  { return MUL; }
  "/"  { return DIV; }
  "%"  { return MOD; }
  "**" { return EXP; }

  "++" { return INC; }
  "--" { return DEC; }

  "!"  { return NOT; }
  "==" { return EQ; }
  "!=" { return NEQ; }
  "<"  { return LESS; }
  ">"  { return GREATER; }
  ">=" { return GTE; }
  "<=" { return LTE; }

  "^" { return BINXOR; }
  "|" { return BINOR; }
  "&" { return BINAND; }

  "str" { return STR; }
  "int" { return INT; }
  "float" { return FLOAT; }
  "bool" { return BOOL; }
  "any" { return ANY; }

  {LINE_COMMENT} { return COMMENT; }
  {IDENT}        { return IDENT; }
  {INT_LIT}      { return INT_LIT; }
  {FLOAT_LIT}    { return FLOAT_LIT; }
  {STRING_LIT}   { return STRING_LIT; }

  {WHITE_SPACE}  { return WHITE_SPACE; }
}

[^] { return BAD_CHARACTER; }
