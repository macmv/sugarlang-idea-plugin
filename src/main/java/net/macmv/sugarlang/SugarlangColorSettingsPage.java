package net.macmv.sugarlang;

import java.util.Map;

import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.openapi.options.colors.AttributesDescriptor;
import com.intellij.openapi.options.colors.ColorDescriptor;
import com.intellij.openapi.options.colors.ColorSettingsPage;
import javax.swing.Icon;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class SugarlangColorSettingsPage implements ColorSettingsPage {
  private static final AttributesDescriptor[] DESCRIPTORS = new AttributesDescriptor[]{
    new AttributesDescriptor("Comment", SugarlangSyntaxHighlighter.COMMENT),
    new AttributesDescriptor("Int", SugarlangSyntaxHighlighter.INT_LIT),
    new AttributesDescriptor("Float", SugarlangSyntaxHighlighter.FLOAT_LIT),
    new AttributesDescriptor("String", SugarlangSyntaxHighlighter.STRING_LIT),
    new AttributesDescriptor("Block braces", SugarlangSyntaxHighlighter.BLOCK),
    new AttributesDescriptor("Parenthesis", SugarlangSyntaxHighlighter.PAREN),
    new AttributesDescriptor("Types", SugarlangSyntaxHighlighter.TYPE),
    new AttributesDescriptor("Keywords", SugarlangSyntaxHighlighter.KEYWORD),
    new AttributesDescriptor("Bad value", SugarlangSyntaxHighlighter.BAD_CHARACTER)
  };

  @Nullable
  @Override
  public Icon getIcon() {
    return Sugarlang.ICON_FILE;
  }

  @NotNull
  @Override
  public SyntaxHighlighter getHighlighter() {
    return new SugarlangSyntaxHighlighter();
  }

  @NotNull
  @Override
  public String getDemoText() {
    return "// Hello world\n"
      + "struct MyType {\n"
      + "  a: int,\n"
      + "  b: str,\n"
      + "  c: any,\n"
      + "}\n"
      + "\n"
      + "pub fn main() {\n"
      + "  v = 5\n"
      + "  if v == 6 {\n"
      + "    println(\"whaaaat\")\n"
      + "  } else {\n"
      + "    println(\"yeah I thought so\")\n"
      + "  }\n"
      + "}\n";
  }

  @Nullable
  @Override
  public Map<String, TextAttributesKey> getAdditionalHighlightingTagToDescriptorMap() {
    return null;
  }

  @NotNull
  @Override
  public AttributesDescriptor[] getAttributeDescriptors() {
    return DESCRIPTORS;
  }

  @NotNull
  @Override
  public ColorDescriptor[] getColorDescriptors() {
    return ColorDescriptor.EMPTY_ARRAY;
  }

  @NotNull
  @Override
  public String getDisplayName() {
    return "Sugarlang";
  }
}