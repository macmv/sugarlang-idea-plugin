package net.macmv.sugarlang;

import com.intellij.lang.Language;
import javax.swing.Icon;

public class Sugarlang extends Language {
  public static final Sugarlang INSTANCE  = new Sugarlang();
  public static final Icon      ICON_FILE = null;
  // public static final Icon ICON_FILE = IconLoader.getIcon("/icons/jar-gray.png");

  protected Sugarlang() {
    super("Sugarlang");
  }
}
