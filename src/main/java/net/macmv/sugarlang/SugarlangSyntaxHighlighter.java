package net.macmv.sugarlang;

import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors;
import com.intellij.openapi.editor.HighlighterColors;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IElementType;
import com.intellij.psi.tree.TokenSet;
import net.macmv.sugarlang.psi.SugarlangTypes;
import org.jetbrains.annotations.NotNull;

public class SugarlangSyntaxHighlighter extends SyntaxHighlighterBase {
  public static final TokenSet KEYWORDS = TokenSet
    .create(SugarlangTypes.IF, SugarlangTypes.ELSE, SugarlangTypes.FOR, SugarlangTypes.WHILE, SugarlangTypes.LOOP, SugarlangTypes.PUB,
      SugarlangTypes.FN, SugarlangTypes.STRUCT, SugarlangTypes.MAP
    );

  public static final TokenSet TYPES = TokenSet
    .create(SugarlangTypes.STR, SugarlangTypes.INT, SugarlangTypes.FLOAT, SugarlangTypes.BOOL, SugarlangTypes.ANY);

  public static final TextAttributesKey INT_LIT       = TextAttributesKey
    .createTextAttributesKey("SUGARLANG_INT_LIT", DefaultLanguageHighlighterColors.NUMBER);
  public static final TextAttributesKey FLOAT_LIT     = TextAttributesKey
    .createTextAttributesKey("SUGARLANG_FLOAT_LIT", DefaultLanguageHighlighterColors.NUMBER);
  public static final TextAttributesKey STRING_LIT    = TextAttributesKey
    .createTextAttributesKey("SUGARLANG_STRING_LIT", DefaultLanguageHighlighterColors.STRING);
  public static final TextAttributesKey KEYWORD       = TextAttributesKey
    .createTextAttributesKey("SUGARLANG_KEYWORD", DefaultLanguageHighlighterColors.KEYWORD);
  public static final TextAttributesKey TYPE          = TextAttributesKey
    .createTextAttributesKey("SUGARLANG_TYPE", DefaultLanguageHighlighterColors.INSTANCE_FIELD);
  public static final TextAttributesKey PAREN         = TextAttributesKey
    .createTextAttributesKey("SUGARLANG_PAREN", DefaultLanguageHighlighterColors.PARENTHESES);
  public static final TextAttributesKey BLOCK         = TextAttributesKey
    .createTextAttributesKey("SUGARLANG_BLOCK", DefaultLanguageHighlighterColors.BRACKETS);
  public static final TextAttributesKey COMMENT       = TextAttributesKey
    .createTextAttributesKey("SUGARLANG_COMMENT", DefaultLanguageHighlighterColors.LINE_COMMENT);
  public static final TextAttributesKey BAD_CHARACTER = TextAttributesKey
    .createTextAttributesKey("SUGARLANG_BAD_CHARACTER", HighlighterColors.BAD_CHARACTER);

  @NotNull
  @Override
  public Lexer getHighlightingLexer() {
    return new SugarlangLexerAdapter();
  }

  @NotNull
  @Override
  public TextAttributesKey @NotNull [] getTokenHighlights(IElementType tok) {
    if (tok.equals(SugarlangTypes.INT_LIT)) {
      return new TextAttributesKey[]{INT_LIT};
    } else if (tok.equals(SugarlangTypes.FLOAT_LIT)) {
      return new TextAttributesKey[]{FLOAT_LIT};
    } else if (tok.equals(SugarlangTypes.STRING_LIT)) {
      return new TextAttributesKey[]{STRING_LIT};
    } else if (tok.equals(SugarlangTypes.OPEN_PAREN) || tok.equals(SugarlangTypes.CLOSE_PAREN)) {
      return new TextAttributesKey[]{PAREN};
    } else if (tok.equals(SugarlangTypes.OPEN_BLOCK) || tok.equals(SugarlangTypes.CLOSE_BLOCK)) {
      return new TextAttributesKey[]{BLOCK};
    } else if (tok.equals(SugarlangTypes.COMMENT)) {
      return new TextAttributesKey[]{COMMENT};
    } else if (tok.equals(TokenType.BAD_CHARACTER)) {
      return new TextAttributesKey[]{BAD_CHARACTER};
    } else if (TYPES.contains(tok)) {
      return new TextAttributesKey[]{TYPE};
    } else if (KEYWORDS.contains(tok)) {
      return new TextAttributesKey[]{KEYWORD};
    } else {
      return new TextAttributesKey[]{};
    }
  }
}
