package net.macmv.sugarlang;

import com.intellij.openapi.fileTypes.LanguageFileType;
import com.intellij.openapi.util.NlsContexts.Label;
import com.intellij.openapi.util.NlsSafe;
import javax.swing.Icon;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class SugarlangFileType extends LanguageFileType {
  public static final SugarlangFileType INSTANCE = new SugarlangFileType();

  protected SugarlangFileType() {
    super(Sugarlang.INSTANCE);
  }

  @Override
  public @NonNls @NotNull String getName() {
    return "Sugarlang";
  }

  @Override
  public @Label @NotNull String getDescription() {
    return "A sugarlang file";
  }

  @Override
  public @NlsSafe @NotNull String getDefaultExtension() {
    return "sug";
  }

  @Override
  public @Nullable Icon getIcon() {
    return Sugarlang.ICON_FILE;
  }
}
