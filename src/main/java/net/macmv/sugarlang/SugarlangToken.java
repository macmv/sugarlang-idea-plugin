package net.macmv.sugarlang;

import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

public class SugarlangToken extends IElementType {
  public SugarlangToken(@NotNull @NonNls String debugName) {
    super(debugName, Sugarlang.INSTANCE);
  }
}
