package net.macmv.sugarlang;

import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

public class SugarlangElem extends IElementType {
  public SugarlangElem(@NotNull @NonNls String debugName) {
    super(debugName, Sugarlang.INSTANCE);
  }
}
