package net.macmv.sugarlang;

import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.psi.FileViewProvider;
import org.jetbrains.annotations.NotNull;

public class SugarlangFile extends PsiFileBase {
  protected SugarlangFile(@NotNull FileViewProvider viewProvider) {
    super(viewProvider, Sugarlang.INSTANCE);
  }

  @NotNull
  @Override
  public SugarlangFileType getFileType() {
    return SugarlangFileType.INSTANCE;
  }

  @Override
  public String toString() {
    return "Sugarlang";
  }
}
